//
// Copyright Institute of Automotive Engineering
// of Technical University of Darmstadt 2020.
// Licensed under the EUPL-1.2-or-later
//
// This work covered by the EUPL can be used/merged and distributed
// in other works covered by GPL-2.0, GPL-3.0, LGPL, AGPL, CeCILL,
// OSL, EPL, MPL and other licences listed as compatible in the EUPL
// Appendix. This applies to the other (combined) work, while the
// original project stays covered by the EUPL without re-licensing.
//
// Alternatively, the contents of this file may be used under the
// terms of the Mozilla Public License, v. 2.0. If a copy of the MPL
// was not distributed with this file, you can obtain one at
// http://mozilla.org/MPL/2.0/.
//

#ifndef Speed_of_Light
#define Speed_of_Light 299792458
#endif

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include "ros_reflections/ros_reflections.hpp"
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <string>
#include <utility>

#ifdef _WIN32
#include <math.h>
#else
#include <cmath>
#endif

using namespace model;
using namespace osi3;

void ros_reflections::apply(SensorData &sensor_data) {
    log("Starting ROS output for reflections");

    ros::param::set("/use_sim_time", true);

    if ((sensor_data.sensor_view_size()==0) || (!sensor_data.has_feature_data())) {
        log("No sensor view or feature data received");
        return;
    }

    if (!sensor_data.sensor_view(0).has_global_ground_truth()) {
        log("No global ground truth received");
        return;
    }

    if (sensor_data.sensor_view(0).lidar_sensor_view().size() > 0) {
        for (size_t sensor_no = 0; sensor_no < sensor_data.sensor_view(0).lidar_sensor_view().size(); sensor_no++) {
            worker_pcl = std::make_unique<reflections::WorkerPCL>("reflections_" + std::to_string(sensor_no), "sensor_" + std::to_string(sensor_no));
            worker_pcl->injectLidar(sensor_data, sensor_no, profile, log);
        }
    } else if (sensor_data.sensor_view(0).radar_sensor_view().size() > 0) {
        for (size_t sensor_no = 0; sensor_no < sensor_data.sensor_view(0).radar_sensor_view().size(); sensor_no++) {
            worker_pcl = std::make_unique<reflections::WorkerPCL>("reflections_" + std::to_string(sensor_no), "sensor_" + std::to_string(sensor_no));
            worker_pcl->injectRadar(sensor_data, sensor_no, profile, log);
        }
    }
}

ros_reflections::ros_reflections(const Profile &profile, const Log &log, const Alert &alert): model::Strategy(profile, log, alert) {
	auto remapping = std::map<std::string, std::string>();
	remapping.emplace("__master", "http://localhost:11311");
	ros::init(remapping, "sensor_model_fmu");
}

reflections::WorkerPCL::WorkerPCL(const std::string& topic, std::string frame_id) : publisher(node.advertise<pcl::PointCloud<pcl::PointXYZ>>(topic, 1)) , frame_id(std::move(frame_id)) {}

void reflections::WorkerPCL::injectLidar(SensorData &sensor_data, size_t sensor_no, const Profile &profile, const Log &log) {
    const auto &lidar_sensor_view = sensor_data.sensor_view(0).lidar_sensor_view(sensor_no);
    if (lidar_sensor_view.reflection().size() > 0) {
        //get raytracing configuration
        auto number_of_rays_horizontal = profile.sensor_view_configuration.lidar_sensor_view_configuration(0).number_of_rays_horizontal();
        //auto horizontal_angle_min_rad = -profile.sensor_view_configuration.lidar_sensor_view_configuration(sensor_no).field_of_view_horizontal() / 2;
        auto horizontal_angle_min_rad = profile.detection_sensing_parameters.horizontal_angle_min_rad[sensor_no];
        auto beam_height_rad = profile.detection_sensing_parameters.beam_height_rad[sensor_no];
        auto beam_width_rad = profile.detection_sensing_parameters.beam_width_rad[sensor_no];
        auto ray_resolution_vertical_rad = profile.detection_sensing_parameters.ray_resolution_vertical_rad[sensor_no];
        auto ray_resolution_horizontal_rad = profile.detection_sensing_parameters.ray_resolution_horizontal_rad[sensor_no];
        auto vertical_start_angle_top_left_rad = profile.detection_sensing_parameters.beam_layer_angles_rad.at(0).at(0) + beam_height_rad / 2;
        auto horizontal_start_angle_top_left_rad = horizontal_angle_min_rad;
        auto max_range = profile.sensor_view_configuration.range();

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tmp(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>());
        //        auto time = ros::Time::now();
        auto sim_seconds = uint32_t(sensor_data.sensor_view(0).global_ground_truth().timestamp().seconds());
        auto sim_nanos = uint32_t(sensor_data.sensor_view(0).global_ground_truth().timestamp().nanos());
        //auto sim_time = ros::Time(sim_seconds, sim_nanos);
        auto sim_time_behind = ros::Time(sim_seconds, sim_nanos + 500);
        pcl_conversions::toPCL(sim_time_behind, cloud_tmp->header.stamp);
        cloud_tmp->header.frame_id = frame_id;

        //run through all reflections
        size_t row_no;
        auto no_of_reflections = lidar_sensor_view.reflection_size();
        size_t print_idx = 0;
        for (int reflection_idx = 0; reflection_idx < no_of_reflections; reflection_idx++) {
            // Calculate distance in m from ToF in s
            double distance = lidar_sensor_view.reflection(reflection_idx).time_of_flight() * Speed_of_Light * 0.5;
            if (distance > 0 && distance < max_range) {
                row_no = size_t(reflection_idx / number_of_rays_horizontal);
                double azimuth = horizontal_start_angle_top_left_rad + double(reflection_idx - number_of_rays_horizontal * row_no) * ray_resolution_horizontal_rad;
                double elevation = vertical_start_angle_top_left_rad - (float)row_no * ray_resolution_vertical_rad;

                double x = distance * cos(azimuth) * cos(elevation);
                double y = distance * sin(azimuth) * cos(elevation);
                double z = distance * sin(elevation);
                cloud_tmp->points.emplace_back(pcl::PointXYZ(float(x), float(y), float(z)));
            }
        }
        pcl::copyPointCloud(*cloud_tmp, *cloud);
        for (int reflection_idx = 0; reflection_idx < no_of_reflections; reflection_idx++) {
            double distance = lidar_sensor_view.reflection(reflection_idx).time_of_flight() * Speed_of_Light * 0.5;
            if (distance > 0 && distance < max_range) {
                /// Calculate intensity in % from signal_strength in dB (linear scaling: -0 dB = 100 %, -110 dB = 0 %)
                double intensity = 100 + lidar_sensor_view.reflection(reflection_idx).signal_strength() / 110 * 100;
                cloud->points[print_idx].intensity = float(intensity);
                print_idx++;
            }
        }
        publisher.publish(cloud);
    } else {
        auto timestamp = (double) sensor_data.sensor_view(0).global_ground_truth().timestamp().seconds() + (double) sensor_data.sensor_view(0).global_ground_truth().timestamp().nanos() / 1000000000;
        log("No reflections from sensor " + std::to_string(sensor_no) + " for ROS output at timestamp " + std::to_string(timestamp));
    }
}

void reflections::WorkerPCL::injectRadar(SensorData &sensor_data, size_t sensor_no, const Profile &profile, const Log &log) {
    //loop over all sensors and rendering results
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tmp(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>());
    //        auto time = ros::Time::now();
    auto sim_seconds = uint32_t(sensor_data.sensor_view(0).global_ground_truth().timestamp().seconds());
    auto sim_nanos = uint32_t(sensor_data.sensor_view(0).global_ground_truth().timestamp().nanos());
    //auto sim_time = ros::Time(sim_seconds, sim_nanos);
    auto sim_time_behind = ros::Time(sim_seconds, sim_nanos + 500);
    pcl_conversions::toPCL(sim_time_behind, cloud_tmp->header.stamp);
    cloud_tmp->header.frame_id = frame_id;
    /// Loop over all received sensor views

    for (auto &sensor_view: sensor_data.sensor_view()) {
        /// Loop over all received radar sensors per sensor view
        size_t radar_sensor_idx = 0;
        for (auto &radar_sensor_view: sensor_view.radar_sensor_view()) {
            /// Run through all rendering results
            int reflection_idx = 0;
            for (auto &reflection: radar_sensor_view.reflection()) {
                auto ray_vertical_angle_rad = reflection.source_vertical_angle();
                auto ray_horizontal_angle_rad = reflection.source_horizontal_angle();
                auto distance = reflection.time_of_flight() * Speed_of_Light / 2;

                double x = distance * cos(ray_horizontal_angle_rad) * cos(ray_vertical_angle_rad);
                double y = distance * sin(ray_horizontal_angle_rad) * cos(ray_vertical_angle_rad);
                double z = distance * sin(ray_vertical_angle_rad);
                cloud_tmp->points.emplace_back(pcl::PointXYZ(float(x), float(y), float(z)));

                reflection_idx++;
            }

            pcl::copyPointCloud(*cloud_tmp, *cloud);
            for (reflection_idx = 0;
                 reflection_idx < radar_sensor_view.reflection_size(); reflection_idx++) {
                auto signal_strength_in_dBm = radar_sensor_view.reflection(reflection_idx).signal_strength();
                cloud->points[reflection_idx].intensity = float(signal_strength_in_dBm);
            }
            radar_sensor_idx++;
        }
    }
    publisher.publish(cloud);
}
